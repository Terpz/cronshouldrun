require('./dateFormat');
require('./replaceArray');

function Cron (options) {
}

Cron.prototype.shouldRun = function (time, cron) {
  var time = time.format('MM H d m wd').split(' ');
  var cron = cron.split(' ');
  for (i = 0; i < cron.length; i++) {
    time[i] = time[i].replace('^0+(?=[0-9])','');
    var k = cron[i].split(',');
    for (o = 0; o < k.length; o++) {
      var search = [ '^\\*$', '^([0-9]+)$', '^([0-9]+)\-([0-9]+)$', '^\\*\/([0-9]+)$' ];
      var replace = [ 'true', time[i]+'===$1', '($1<='+time[i]+' && '+time[i]+'<=$2)', time[i]+'%$1===0' ];
      var v = k[o];
      v = v.replaceArray(search,replace);
      k[o] = v;
    }
    cron[i] = '(' + k.join(' || ') + ')';
  }
  var j = cron.join(' && ');
  return eval(j);
}


module.exports = new Cron();
