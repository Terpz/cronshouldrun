# About
Simple module to evaulate, if a cron expression, should run on the giving time:


# Example
```
var cron = require('cronshouldrun');

console.log(cron.shouldRun(new Date(), '* * * * *')); // will always return true

console.log(cron.shouldRun(new Date(), '20 12 * * *')); // will return true, if the clock is 12:20

console.log(cron.shouldRun(new Date(), '*/2 * * * *')); // will return true, every 2 mintues

console.log(cron.shouldRun(new Date(), '10-20 12 * * *')); // will return true within 12:10 - 12:20
```

# Important
This was designed to be run, in a timer, every minute, because it validates on minute interval, and not secounds